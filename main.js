// 1
// Створення об'єкта product
let product = {
    name: 'Футболка',
    price: 100, 
    discount: 20, 
    calculateTotalPrice: function() {
      let discountedPrice = this.price * (1 - this.discount / 100); 
      return discountedPrice;
    }
  };
  
 
  console.log(`Повна ціна товару "${product.name}" з урахуванням знижки: ${product.calculateTotalPrice()} грн`);
  

//   2

// Функція greeting, яка приймає об'єкт з властивостями name та age
function greeting(person) {
    return `Привіт, мені ${person.age} років`;
  }
  let name = prompt("Введіть ваше ім'я:");
  let age = prompt("Введіть ваш вік:");
  let person = { name: name, age: age };
    alert(greeting(person));
  

//   3
function deepClone(obj) {
    if (obj===null || typeof obj !== 'object') {
        return obj;
    }
    let clone = Array.isArray(obj) ? [] : {};
    for (let key in obj) {
        if (obj.hasOwnProperty(key)) {
            clone[key] = deepClone(obj[key]);
        }
    }
    return clone ;
}
